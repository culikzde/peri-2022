QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

SOURCES += \
    mainwindow.cpp

HEADERS += \
    mainwindow.h

FORMS += \
    mainwindow.ui

CONFIG += link_pkgconfig
PKGCONFIG += SoQt

# QMAKE_CXXFLAGS += $$(CXXFLAGS)
# QMAKE_CFLAGS += $$(CFLAGS)
# QMAKE_LFLAGS += -lSoQt

# Ubuntu 20.04
# LIBS += -lSoQt -lCoin

# dnf install SoQt-devel
# apt install libsoqt520-dev

