#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QApplication>

#include <Inventor/Qt/SoQt.h>
#include <Inventor/Qt/viewers/SoQtExaminerViewer.h>
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoCube.h>
#include <Inventor/nodes/SoCone.h>
#include <Inventor/nodes/SoSphere.h>
#include <Inventor/nodes/SoCylinder.h>
#include <Inventor/nodes/SoTransform.h>
#include <Inventor/nodes/SoMaterial.h>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->hsplitter->setStretchFactor (0, 1);
    ui->hsplitter->setStretchFactor (1, 5);
    ui->hsplitter->setStretchFactor (2, 1);

    ui->vsplitter->setStretchFactor (0, 5);
    ui->vsplitter->setStretchFactor (1, 1);

    SoQtExaminerViewer * examiner = new SoQtExaminerViewer (ui->widget);

    SoSeparator * root = new SoSeparator;

    // cone
    SoSeparator * group1 = new SoSeparator;

    SoTransform * shift1 = new SoTransform;
    shift1->translation.setValue (-2.0, 0.0, 0.0);
    group1->addChild (shift1);

    SoCube * block = new SoCube ();
    block->height = 0.2;
    group1->addChild (block);

    SoMaterial * redMaterial = new SoMaterial;
    redMaterial->diffuseColor.setValue (1.0, 0.0, 0.0);
    group1->addChild (redMaterial);

    SoCone * cone = new SoCone ();
    group1->addChild (cone);

    root->addChild (group1);

    // sphere
    SoSeparator * group2 = new SoSeparator;

    SoMaterial * greenMaterial = new SoMaterial;
    greenMaterial->diffuseColor.setValue (0.0, 1.0, 0.0);
    group2->addChild (greenMaterial);

    SoSphere * sphere = new SoSphere ();
    group2->addChild (sphere);

    root->addChild (group2);

    // cube
    SoSeparator * group3 = new SoSeparator;

    SoTransform * shift3 = new SoTransform ();
    shift3->translation.setValue (1.6, 0.0, 1.6);
    shift3->scaleFactor.setValue (0.7, 0.7, 0.7);
    group3->addChild (shift3);

    SoMaterial * blueMaterial = new SoMaterial;
    blueMaterial->diffuseColor.setValue (0.0, 0.0, 1.0);
    group3->addChild (blueMaterial);

    SoCube * cube = new SoCube ();
    group3->addChild (cube);

    root->addChild (group3);

    examiner->setSceneGraph(root);
}

MainWindow::~MainWindow()
{
    delete ui;
}


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    SoQt::init ((QWidget*) NULL); /* doplneno*/
    MainWindow w;
    w.show();
    return a.exec();
}
